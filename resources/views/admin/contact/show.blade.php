@extends('layouts.app')

@section('title','Contact')

@push('css')



@endpush

@section('content')


    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    @include('layouts.partial.message')

                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h3 class="card-title ">Message Details</h3>
                            <p class="card-category"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, perferendis.</p>
                        </div>
                        <div class="card-content mt-5">
                            <div class="row">
                                <div class="col-md-10 ml-3">

                                    <strong>Name : {{ $contacts->name }} </strong><br>

                                    <b>E-mail : {{ $contacts->email }}</b><br>

                                    <strong>Message : </strong>
                                    <hr>
                                    <p>{{ $contacts->message }}</p>
                                    <hr>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>




@endsection

@push('scripts')



@endpush
