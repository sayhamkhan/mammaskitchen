@extends('layouts.app')

@section('title','Contact')

@push('css')

	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

@endpush

@section('content')


	<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              @include('layouts.partial.message')

              <div class="card">
                <div class="card-header card-header-primary">
                  <h3 class="card-title ">All Contact Message</h3>
                  <p class="card-category"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, perferendis.</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="myTable" class="table table-striped table-bordered " style="width:100%;">
                      <thead class=" text-primary">
                        <th style="width: 50px;"> ID </th>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Subject </th>
                        <th> Message </th>
                        <th> Action </th>
                      </thead>
                      <tbody>
                      @foreach($contacts as $key=>$contact)


                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$contact->name}}</td>
                            <td>{{$contact->email}}</td>
                            <td>{{$contact->subject}}</td>
                            <td>{{$contact->message}}</td>
{{--                            <td><i style="cursor: pointer; width: 100%;" class="material-icons text-danger">delete</i></td>--}}
                            <td class="text-center">

                                <a href="{{route('contact.show', $contact->id)}}"><i class="material-icons mr-3 text-warning mb-3"> details </i></a>

                                <form id="delete-form-{{$contact->id}}" action="{{ route('contact.destroy', $contact->id) }}" style="display: none;" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                                <button type="button" style="border:none;background: none;" onclick="if (confirm('Are you sure? You want to delete this Item!')) {
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{$contact->id}}').submit();
                                    }else{
                                    event.preventDefault();
                                    }"><i style="cursor: pointer; width: 100%;" class="material-icons text-danger"> delete </i>
                                </button>

                            </td>
{{--          					<td class="text-center">--}}


{{--                            <form id="delete-form-{{$contact->id}}" action="{{ route('') }}" style="display: none;" method="POST">--}}
{{--                              @csrf--}}
{{--                              @method('DELETE')--}}
{{--                            </form>--}}
{{--                            <button type="button" style="border:none;background: none;" onclick="if (confirm('Are you sure? You want to delete this Reservation!')) {--}}
{{--                                  event.preventDefault();--}}
{{--                                  document.getElementById('delete-form-{{$contact->id}}').submit();--}}
{{--                                }else{--}}
{{--                                  event.preventDefault();--}}
{{--                                }"><i style="cursor: pointer; width: 100%;" class="material-icons text-danger">delete</i>--}}
{{--                            </button>--}}

{{--                            </td>--}}
                        </tr>


                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




@endsection

@push('scripts')

	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

	<script>
		$(document).ready(function() {
		    $('#myTable').DataTable();
		} );
	</script>

@endpush
