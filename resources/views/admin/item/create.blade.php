@extends('layouts.app')

@section('title','Item')

@push('css')
	

	
@endpush

@section('content')


	<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              @include('layouts.partial.message')

              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Add Item</h4>
                  <p class="card-category"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, perferendis.</p>
                </div>
                <div class="card-body">
                  
                    <form method="POST" action="{{route('item.store')}}" enctype="multipart/form-data">
                      @csrf
                        <div class="col-md-12">
                          <div class="form-group label-floating">
                            <label class="bmd-label-floating">Item Name</label>
                            <input type="text" class="form-control" name="name">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group label-floating">
                            <label class="bmd-label-floating">Select Category</label>
                            <select name="category" class="form-control">
                              @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group label-floating">
                            <label class="bmd-label-floating">Description</label>
                            <input type="text" class="form-control" name="description">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group label-floating">
                            <label class="bmd-label-floating">Price</label>
                            <input type="number" class="form-control" name="price">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <label class="bmd-label-floating">Image</label>
                          <input type="file" class="form-control" name="image">
                        </div>
                        <br>

                        <a href="{{route('item.index')}}" class="btn btn-warning">Back</a>
                        <button type="submit" class="btn btn-primary">Save this</button>
                        
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




@endsection

@push('scripts')
	
	
@endpush