@extends('layouts.app')

@section('title','Item')

@push('css')

	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


@endpush

@section('content')


	<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <a href="{{route('item.create')}}" class="btn btn-info">Add New Item</a>

              @include('layouts.partial.message')

              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">All Item</h4>
                  <p class="card-category"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, perferendis.</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="myTable" class="table table-striped table-bordered " style="width:100%;">
                      <thead class=" text-primary">
                        <th style="width: 50px;"> ID </th>
                        <th> Item Name </th>
                        <th> Image </th>
                        <th> Category </th>
                        <th style="width: 320px;"> Description </th>
                        <th style="width: 65px;"> Price </th>
                        <th> Action </th>
                      </thead>
                      <tbody>
                            @foreach($items as $key=>$item)
                            <tr>
              								<td>{{ $key+1 }}</td>
              								<td>{{ $item->name }}</td>
              								<td>
                                <img class="img-responsive img-thumbnail" src="{{ asset('upload/item/'.$item->image) }}" alt="" style="width: 100px; height: 100px;">
                              </td>
                              <td>{{ $item->itemCategory->name }}</td>
                              <td>{{ $item->description }}</td>
                              <td>{{ $item->price }}</td>
                                <td class="text-center">

                                <a href="{{route('item.edit', $item->id)}}"><i class="material-icons mr-3 text-warning mb-3"> edit </i></a>

                                <form id="delete-form-{{$item->id}}" action="{{route('item.destroy', $item->id)}}" style="display: none;" method="POST">
                                  @csrf
                                  @method('DELETE')
                                </form>
                                  <button type="button" style="border:none;background: none;" onclick="if (confirm('Are you sure? You want to delete this Item!')) {
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{$item->id}}').submit();
                                      }else{
                                        event.preventDefault();
                                      }"><i style="cursor: pointer; width: 100%;" class="material-icons text-danger"> delete </i>
                                  </button>

                              </td>
              							</tr>
                            @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




@endsection

@push('scripts')

	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

	<script>
		$(document).ready(function() {
		    $('#myTable').DataTable();
		} );
	</script>

@endpush
