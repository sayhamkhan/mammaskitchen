@extends('layouts.app')

@section('title','Slider')

@push('css')
	

	
@endpush

@section('content')


	<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              @if ($errors->any())
                      
                    @include('layouts.partial.message')

              @endif

              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Edit Slider</h4>
                  <p class="card-category"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, perferendis.</p>
                </div>
                <div class="card-body">
                  
                    <form method="POST" action="{{route('slider.update', $slider->id)}}" enctype="multipart/form-data">
                      @csrf

                      @method('PUT')

                        <div class="col-md-12">
                          <div class="form-group label-floating">
                            <label class="bmd-label-floating">Title</label>
                            <input type="text" class="form-control" name="title" value="{{$slider->title}}">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group label-floating">
                            <label class="bmd-label-floating">Sub Title</label>
                            <input type="text" class="form-control" name="sub_title" value="{{$slider->sub_title}}">
                          </div>
                        </div>
                        
                        <div class="col-md-12">
                          <label class="bmd-label-floating">Image</label>
                          <input type="file" class="form-control" name="image">
                        </div>
                        <br>
                        <a href="{{route('slider.index')}}" class="btn btn-warning">Back</a>
                        <button type="submit" class="btn btn-primary">Update Slider</button>
                        

                        
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




@endsection

@push('scripts')
	
	
@endpush