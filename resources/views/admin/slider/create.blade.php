@extends('layouts.app')

@section('title','Slider')

@push('css')
	

	
@endpush

@section('content')


	<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              @include('layouts.partial.message')

              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Add Slider</h4>
                  <p class="card-category"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, perferendis.</p>
                </div>
                <div class="card-body">
                  
                    <form method="POST" action="{{route('slider.store')}}" enctype="multipart/form-data">
                      @csrf
                        <div class="col-md-12">
                          <div class="form-group label-floating">
                            <label class="bmd-label-floating">Title</label>
                            <input type="text" class="form-control" name="title">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group label-floating">
                            <label class="bmd-label-floating">Sub Title</label>
                            <input type="text" class="form-control" name="sub_title">
                          </div>
                        </div>
                        
                        <div class="col-md-12">
                          <label class="bmd-label-floating">Image</label>
                          <input type="file" class="form-control" name="image">
                        </div>
                        <br>
                        <a href="{{route('slider.index')}}" class="btn btn-warning">Back</a>
                        <button type="submit" class="btn btn-primary">Add Slider</button>
                        

                        
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




@endsection

@push('scripts')
	
	
@endpush