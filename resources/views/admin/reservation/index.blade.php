@extends('layouts.app')

@section('title','Reservation')

@push('css')
	
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
	
@endpush

@section('content')


	<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              @include('layouts.partial.message')

              <div class="card">
                <div class="card-header card-header-primary">
                  <h3 class="card-title ">Reservations</h3>
                  <p class="card-category"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, perferendis.</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="myTable" class="table table-striped table-bordered " style="width:100%;">
                      <thead class=" text-primary">
                        <th style="width: 50px;"> ID </th>
                        <th> Name </th>
                        <th> Phone </th>
                        <th> Email </th>
                        <th> Date & Time </th>
                        <th> Message </th>
                        <th> Status </th>
                        <th> Action </th>
                      </thead>
                      <tbody>
                      @foreach($reservations as $key=>$reservation)


          							<tr>
          								<td>{{$key+1}}</td>
          								<td>{{$reservation->name}}</td>
          								<td>{{$reservation->phone}}</td>
          								<td>{{$reservation->email}}</td>
                          <td>{{$reservation->date_and_time}}</td>
                          <td>{{$reservation->message}}</td>
                          <td>
                            @if($reservation->status == true)
                              <span class="badge badge-info">Confirmed</span>
                            @else
                              <span class="badge badge-danger">Processing</span>
                            @endif
                          </td>
          								<td class="text-center">

                            @if($reservation->status == false)
                              <form id="delete-form-{{$reservation->id}}" action="{{ route('reservation.status', $reservation->id) }}" style="display: none;" method="POST">
                              @csrf
                            </form>
                            <button type="button" style="border:none;background: none;" onclick="if (confirm('Did you confirm this reservation by phone ???')) {
                                  event.preventDefault();
                                  document.getElementById('delete-form-{{$reservation->id}}').submit();
                                }else{
                                  event.preventDefault();
                                }"><i style="cursor: pointer; width: 100%;" class="material-icons text-info">done</i>
                            </button>
                            @endif

                            <form id="delete-form-{{$reservation->id}}" action="{{ route('reservation.destroy', $reservation->id) }}" style="display: none;" method="POST">
                              @csrf
                              @method('DELETE')
                            </form>
                            <button type="button" style="border:none;background: none;" onclick="if (confirm('Are you sure? You want to delete this Reservation!')) {
                                  event.preventDefault();
                                  document.getElementById('delete-form-{{$reservation->id}}').submit();
                                }else{
                                  event.preventDefault();
                                }"><i style="cursor: pointer; width: 100%;" class="material-icons text-danger">delete</i>
                            </button>
                              
                            </td>
          							  </tr>


                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




@endsection

@push('scripts')
	
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

	<script>
		$(document).ready(function() {
		    $('#myTable').DataTable();
		} );
	</script>
	
@endpush