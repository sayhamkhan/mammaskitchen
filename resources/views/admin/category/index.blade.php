@extends('layouts.app')

@section('title','Category')

@push('css')
	
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
	
@endpush

@section('content')


	<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <a href="{{route('category.create')}}" class="btn btn-info">Add New Category</a>

              @include('layouts.partial.message')

              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">All Category</h4>
                  <p class="card-category"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, perferendis.</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="myTable" class="table table-striped table-bordered " style="width:100%;">
                      <thead class=" text-primary">
                        <th>
                          ID  
                        </th>
                        <th>
                          Category Name
                        </th>
                        <th>
                          Slug
                        </th>
                        <th>
                          Action
                        </th>
                      </thead>
                      <tbody>
              @foreach($categories as $key=>$category)


							<tr>
								<td>{{$key+1}}</td>
								<td>{{$category->name}}</td>
								<td>{{$category->slug}}</td>
								<td class="text-center">

                  <a href="{{route('category.edit', $category->id)}}"><i class="material-icons mr-3 text-warning mb-3">edit</i></a>

                  <form id="delete-form-{{$category->id}}" action="{{route('category.destroy', $category->id)}}" style="display: none;" method="POST">
                    @csrf
                    @method('DELETE')
                  </form>
                  <button type="button" style="border:none;background: none;" onclick="if (confirm('Are you sure? You want to delete this Category!')) {
                        event.preventDefault();
                        document.getElementById('delete-form-{{$category->id}}').submit();
                      }else{
                        event.preventDefault();
                      }"><i style="cursor: pointer; width: 100%;" class="material-icons text-danger">delete</i>
                  </button>
                    
                </td>
							</tr>


                         @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




@endsection

@push('scripts')
	
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

	<script>
		$(document).ready(function() {
		    $('#myTable').DataTable();
		} );
	</script>
	
@endpush