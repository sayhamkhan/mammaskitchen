@extends('layouts.app')

@section('title','Category')

@push('css')
	

	
@endpush

@section('content')


	<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

              @include('layouts.partial.message')

              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Add Category</h4>
                  <p class="card-category"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, perferendis.</p>
                </div>
                <div class="card-body">
                  
                    <form method="POST" action="{{route('category.store')}}" enctype="multipart/form-data">
                      @csrf
                        <div class="col-md-12">
                          <div class="form-group label-floating">
                            <label class="bmd-label-floating">Category Name</label>
                            <input type="text" class="form-control" name="name">
                          </div>
                        </div>
                        <br>
                        <a href="{{route('category.index')}}" class="btn btn-warning">Back</a>
                        <button type="submit" class="btn btn-primary">Add Category</button>
                        

                        
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




@endsection

@push('scripts')
	
	
@endpush