<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Models\Category;
use App\Models\Item;

class HomeController extends Controller
{
   
    public function index()
    {
    	$categories = Category::all();
    	$items = Item::all();
        $sliders = Slider::all();
        return view('welcome',compact('sliders', 'categories', 'items'));
    }
}
