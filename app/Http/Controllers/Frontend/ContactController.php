<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Contact;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function contact(Request $request)
    {
    	$this->validate($request,[
    		'name' 			=> 'required',
    		'email' 		=> 'required|email',
    		'subject' 		=> 'required',
    		'message' 		=> 'required'
    	]);
        $contact = new Contact();
        $contact->name      = $request->name;
        $contact->email     = $request->email;
        $contact->subject   = $request->subject;
        $contact->message   = $request->message;
        $contact->save();
        Toastr::success('Your message successfully sent. We will contact you soon !!','success',["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }
}
