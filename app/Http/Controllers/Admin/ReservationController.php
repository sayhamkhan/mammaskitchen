<?php

namespace App\Http\Controllers\Admin;
use App\Models\Reservation;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{
    public function index()
    {
    	$reservations = Reservation::all();
    	return view('admin.reservation.index', compact('reservations'));
    }
    public function status($id)
    {
    	 $reservation = Reservation::find($id);
    	 $reservation->status = true;
    	 $reservation->save();
    	 Toastr::success('Reservation successfully confirmed !!','success',["positionClass" => "toast-top-right"]);
    	 return redirect()->back();
    }
    public function destroy($id)
    {
    	Reservation::find($id)->delete();
    	Toastr::success('Reservation successfully deleted !!','success',["positionClass" => "toast-top-right"]);
    	 return redirect()->back();
    }
}
