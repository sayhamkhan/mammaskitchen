<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function contact()
    {
        $contacts = Contact::all();
        return view('admin.contact.index', compact('contacts'));
    }
    public function show($id)
    {
        $contacts = Contact::find($id);
        //dd($contacts);
        return view('admin.contact.show',compact('contacts'));
    }
    public function destroy($id)
    {
        Contact::find($id)->delete();
        Toastr::success('Message successfully deleted!!','success',["positionClass" => "toast-top-right"]);
        return redirect()->back();

    }
}
