<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function itemCategory()
    {
    	return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }
}
